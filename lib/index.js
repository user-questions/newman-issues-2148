function MyNewmanReporter (newman, reporterOptions, collectionRunOptions) {
 
  
    newman.on('beforeDone', () => {
      console.log('before');

      var request = require('request');
      request('http://httpbin.org/get', function (error, response, body) {
        console.log('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for the Google homepage.
      });

      console.log('after');
    });

  }
  module.exports = MyNewmanReporter